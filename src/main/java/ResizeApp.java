import com.google.common.base.Stopwatch;
import com.google.common.io.Files;
import org.apache.log4j.Logger;
import utils.ImageUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.Properties;

/**
 * Created by pierre on 20/06/17.
 */
public class ResizeApp {

    public static final Logger LOGGER = Logger.getLogger(ResizeApp.class);
    public static final Collection<String> EXTENSIONS = Arrays.asList("jpg", "png", "jpeg");
    public static final String GUN_SEPARATOR = "_";
    public static final String GUN_PREFIX = "G_";
    public static final String MEDIA_PATTERN = "G_:ugProduct:_:colorCode:_ZP_:view:.:extension:";


    public static void main(String[] args) {

        Stopwatch timer = Stopwatch.createStarted();
        if (args.length < 1) {
            LOGGER.error("Missing application properties file");
            System.exit(-1);
        }

        Properties properties = null;
        try {
            properties = new Properties();
            properties.load(java.nio.file.Files.newInputStream(Paths.get(args[0])));
        } catch (IOException e) {
            LOGGER.error("Unable to read application properties file", e);
            System.exit(-1);
        }

        String sourceFolderString = properties.getProperty("folder.source");
        String archiveFolderString = properties.getProperty("folder.archive");
        String targerFolderString = properties.getProperty("folder.result");
        final String rejectFolderString = properties.getProperty("folder.reject");

        final File sourceFolder = Paths.get(sourceFolderString).toFile();

        LOGGER.info("*****************************************************");
        LOGGER.info("*************** Starting Image Resize ***************");
        LOGGER.info("*****************************************************");

        final int fileToResize = sourceFolder.listFiles().length;
        Integer mediasInError = 0;
        Integer mediasResized = 0;

        for (File f : sourceFolder.listFiles()) {
            final String fileExtension = Files.getFileExtension(f.getName());
            if (EXTENSIONS.contains(fileExtension) && f.getName().startsWith(GUN_PREFIX)) {

                final String nameWithoutExtension = Files.getNameWithoutExtension(f.getName());
                // G_ugProduct_colorCode_template_view
                final String[] split = nameWithoutExtension.split(GUN_SEPARATOR);
                if (split.length == 5) {

                    //Getting the informations to build the final media name
                    final String ugProduct = split[1];
                    final String colorCode = split[2];
                    final String template = split[3];
                    final String view = split[4];

                    // Getting the template from the enum
                    TemplateEnum templateToUse = null;
                    for (TemplateEnum templateEnum : TemplateEnum.values()) {
                        if (templateEnum.getCode().equals(template)) {
                            templateToUse = templateEnum;
                            break;
                        }
                    }

                    // Checking the template is ok
                    if (templateToUse != null) {

                        try {
                            final Stopwatch imageTimer = Stopwatch.createStarted();
                            LOGGER.info(String.format("Starting to resize %s", f.getName()));
                            final BufferedImage image = ImageIO.read(f);

                            // Resizing of the image based on the template
                            final BufferedImage computedImage = ImageUtils.getComputedImage(image, templateToUse.getWidth(), templateToUse.getHeight(),
                                    templateToUse.getMargin(), 1100, 1200);

                            // One archives the initial media
                            final Path archivedMediaPath = Paths.get(archiveFolderString + "/" + f.getName());
                            Files.move(f, archivedMediaPath.toFile());

                            // One writting the resulting image
                            final String mediaNameFromPattern = getMediaNameFromPattern(ugProduct, colorCode, view, fileExtension);
                            if (mediaNameFromPattern != null) {
                                final Path mediaResultPath = Paths.get(targerFolderString + "/" + mediaNameFromPattern);
                                ImageIO.write(computedImage, fileExtension, mediaResultPath.toFile());
                                mediasResized++;
                                LOGGER.info(String.format("%s resized in %s", f.getName(), imageTimer.stop()));
                            } else {
                                LOGGER.warn(String.format("Incorrect result media file name %s", mediaNameFromPattern));
                                mediasInError++;
                            }
                        } catch (IOException e) {
                            LOGGER.error(String.format("Error while computing image %s", f.getName()), e);
                            rejectFile(f, rejectFolderString);
                            mediasInError++;
                        }

                    } else {
                        LOGGER.warn(String.format("Invalid template {} for file {}", template, f.getName()));
                        rejectFile(f, rejectFolderString);
                        mediasInError++;
                    }

                } else {
                    LOGGER.warn(String.format("Invalid file name %s", f.getName()));
                    rejectFile(f, rejectFolderString);
                    mediasInError++;
                }

            } else {
                LOGGER.warn(String.format("Invalid file %s", f.getName()));
                rejectFile(f, rejectFolderString);
                mediasInError++;
            }
        }
        LOGGER.info("************************************************************************");
        LOGGER.info(String.format("ResizeApp finished in %s", timer.stop()));
        LOGGER.info(String.format("%d medias were succefully resized over %d file(s) with %d media(s) in error",
                mediasResized, fileToResize, mediasInError));
        LOGGER.info("************************************************************************");

    }

    public static String getMediaNameFromPattern(final String ugProduct, final String colorCode, final String view,
                                                 String extension) {
        String mediaName = null;
        if (!ugProduct.isEmpty() && !colorCode.isEmpty() && !view.isEmpty() && !extension.isEmpty()) {
            mediaName = MEDIA_PATTERN;
            mediaName = mediaName.replace(":ugProduct:", ugProduct);
            mediaName = mediaName.replace(":colorCode:", colorCode);
            mediaName = mediaName.replace(":view:", view);
            if (extension.contains(".")) {
                extension = extension.replace(".", "");
            }
            mediaName = mediaName.replace(":extension:", extension);
        }
        return mediaName;
    }

    public static void rejectFile(File file, final String rejectFolderString) {
        final File rejectedFile = Paths.get(rejectFolderString + "/" + file.getName()).toFile();
        try {
            Files.move(file, rejectedFile);
        } catch (IOException e) {
            LOGGER.error(String.format("Error while moving file %s to rejected file %s", file.getName(), rejectedFile.getName()), e);
        }
    }

}
